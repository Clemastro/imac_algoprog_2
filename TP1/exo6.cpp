#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    // your code
    int capacite;
    int taille;
};


void initialise(Liste* liste)
{
    liste->premier=0;
}

bool est_vide(const Liste* liste)
{
    if(liste->premier==0){
        return true;
    }
    else{
        return false;
    }

}

void ajoute(Liste* liste, int valeur)
{
    if(liste->premier==0){
        Noeud *newNode = (Noeud*)malloc(sizeof (*newNode));
        newNode->donnee=valeur;
        newNode->suivant=0;
        liste->premier=newNode;
    }
    else{
        Noeud* currentNode = liste->premier;
        while(currentNode->suivant==0){
            currentNode=currentNode->suivant;
        }
        currentNode->suivant->donnee=valeur;
        currentNode->suivant->suivant=0;
    }
}

void affiche(const Liste* liste)
{
    Noeud* currentNode = liste->premier;
    while(currentNode!=0){
        cout << currentNode->donnee << ' / ';
        currentNode=currentNode->suivant;
    }
    cout << endl;
}

int recupere(const Liste* liste, int n)
{
    Noeud* currentNode = liste->premier;
    for(int i=0; i<n; i++){
        currentNode=currentNode->suivant;
    }
    if(currentNode == 0){
        return 0;
    }
    else{
        return currentNode->donnee;
    }
}

int cherche(const Liste* liste, int valeur)
{
    Noeud* currentNode = liste->premier;
    int id=0;
    bool found = false;
    while(currentNode == 0 || found == false){
        if(currentNode->donnee == valeur){
            found = true;
        }
        else{
            currentNode=currentNode->suivant;
            id++;
        }
    }
    if(found){
        return id;
    }
    else{
        return -1;
    }
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* currentNode = liste->premier;
    for(int i=0; i<n; i++){
        currentNode=currentNode->suivant;
    }
    if(currentNode != 0){
        currentNode->donnee=valeur;
    }
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if(tableau->capacite==tableau->taille){
        tableau->donnees=(int*)malloc(sizeof (int)*(tableau->capacite));
        tableau->capacite++;

    }
    tableau->donnees[tableau->taille]=valeur;
    tableau->taille++;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->donnees=(int*)malloc(sizeof (int)*capacite);
    tableau->capacite=capacite;
    tableau->taille=0;

}

bool est_vide(const DynaTableau* tableau)
{
    if(tableau->taille == 0){
        return true;
    }
    else{
        return false;
    }
}

void affiche(const DynaTableau* tableau)
{
    for(int i = 0; i<tableau->taille; i++){
        cout << tableau->donnees[i] << ' / ';
    }
    cout << endl;
}

int recupere(const DynaTableau* tableau, int n)
{
    if(tableau->taille<n){
        return  0;
    }
    else{
        return tableau->donnees[n];
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int id;
    bool found = false;
    while(id < tableau->taille || found == false){
        if(tableau->donnees[id] == valeur){
            found = true;
        }
        else{
            id++;
        }
    }
    if(found){
        return id;
    }
    else{
        return -1;
    }
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if(n<=tableau->taille){
        tableau->donnees[n]=valeur;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if(est_vide(liste)){
        return 0;
    }
    else{
        int donnee = liste->premier->donnee;
        liste->premier=liste->premier->suivant;
        return donnee;
    }
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(DynaTableau* tableau, int valeur)
{
    ajoute(tableau, valeur);
}

//int retire_pile(DynaTableau* liste)
int retire_pile(DynaTableau* tableau)
{
    if(est_vide(tableau)){
        return 0;
    }
    else{
        int donnee = tableau->donnees[tableau->taille-1];
        tableau->donnees[tableau->taille-1]=0;
        tableau->taille--;
        return donnee;
    }
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste1" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    DynaTableau pile; // DynaTableau pile;
    Liste file; // Liste file;

    initialise(&pile , 0);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&liste) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
