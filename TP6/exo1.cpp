#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */

    for(int i=0; i<nodeCount; i++){
        this->appendNewNode(new GraphNode(i));
    }

    for(int n = 0; n< nodeCount; n++){
        for(int m = 0; m< nodeCount; m++){
            if(adjacencies[n][m] >= 1){
                this->nodes[n]->appendNewEdge(this->nodes[m], adjacencies[n][m]);
            }
        }
    }
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */
    nodes[nodesSize]=first;
    visited[first->value]=true;
    nodesSize++;
    Edge* newEdge = first->edges;

    while(newEdge != 0){
        if(!visited[newEdge->destination->value]){
            deepTravel(newEdge->destination, nodes, nodesSize, visited);
        }
        newEdge=newEdge->next;
    }
}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue;
	nodeQueue.push(first);
    while(!nodeQueue.empty()){
        GraphNode* n = nodeQueue.front();
        nodes[nodesSize]=n;
        nodeQueue.pop();
        nodesSize++;
        for(Edge* e = n->edges; e != NULL; e=e->next){
            if(!visited[e->destination->value]){
                nodeQueue.push(e->destination);
                visited[e->destination->value]=true;
            }
        }
    }
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
    Edge* e = first->edges;
    visited[first->value]=true;
    for(e; e!=NULL; e=e->next){
        if(visited[e->destination->value]){
            return true;
        }
        else{
            detectCycle(e->destination, visited);
        }
    }
    return false;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
