#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	// bubbleSort
    bool changement = true;
    while(changement == true){
        changement=false;
        for(int i = 0; i < toSort.size()-1; i++){
            if(toSort[i]>toSort[i+1]){
                int actualcell=toSort[i];
                toSort[i]=toSort[i+1];
                toSort[i+1]=actualcell;
                changement=true;
            }
        }
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
