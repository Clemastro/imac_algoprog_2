#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());

	// insertion sort from toSort to sorted
    int tab[toSort.size()];
    for(int i=0; i<toSort.size(); i++){
        if(tab[0]>toSort[i]){
            for(int n=tab.size()-1; n>0; n--){
                tab[n+1]=tab[n];
            }
            tab[0]=toSort[i];
        }
        else{
            tab[i]=toSort[i];
        }
    }

	
	toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
