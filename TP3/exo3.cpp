#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->left=0;
        this->right=0;
        this->value=value;

    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        if(this->value>value){
            if(this->left == 0){
                Node* newNode=createNode(value);
                this->left=newNode;
            }
            else{
                this->left->insertNumber(value);
            }
        }
        else{
            if(this->right == 0){
                Node* newNode=createNode(value);
                this->right=newNode;
            }
            else{
                this->right->insertNumber(value);
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        int leftHeight = 1;
        int rightHeight = 1;

        if(this->left != 0){
            leftHeight += this->left->height();
        }
        if(this->right != 0){
            rightHeight += this->right->height();
        }

        if(leftHeight>rightHeight){
            return leftHeight;
        }
        else{
            return rightHeight;
        }
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        int nodeCount= 1;

        if(this->left != 0){
            nodeCount = nodeCount + this->left->height();
        }

        if(this->right != 0){
            nodeCount += nodeCount + this->right->height();
        }

        return nodesCount();
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)

        bool verifLeaf = false;

        if(this->left == 0 && this->right == 0){
            verifLeaf=true;
        }

        return verifLeaf;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(this->isLeaf()){
            leaves[leavesCount]=this;
            leavesCount++;
        }

        else{
            if(this->left != 0){
                this->left->allLeaves(leaves, leavesCount);
            }

            if(this->right != 0){
                this->right->allLeaves(leaves, leavesCount);
            }
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if(this->left != 0){
            this->left->inorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount]=this;
        nodesCount++;

        if(this->right != 0){
            this->right->inorderTravel(nodes,nodesCount);
        }
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel

        nodes[nodesCount]=this;
        nodesCount++;

        if(this->left != 0){
            this->left->preorderTravel(nodes, nodesCount);
        }

        if(this->right != 0){
            this->right->preorderTravel(nodes,nodesCount);
        }
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if(this->left != 0){
            this->left->postorderTravel(nodes, nodesCount);
        }

        if(this->right != 0){
            this->right->postorderTravel(nodes,nodesCount);
        }

        nodes[nodesCount]=this;
        nodesCount++;
	}

	Node* find(int value) {
        // find the node containing value
        if(this->value == value){
            return this;
        }
        else{
            if(this->value>value){
                return this->left->find(value);
            }
            else if(this->value<value){
                return this->right->find(value);
            }
            else{
                return nullptr;
            }
        }
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
